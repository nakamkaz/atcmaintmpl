func ToIntSlice(stra []string) []int {
        var inta = make([]int, len(stra))
        for i, v := range stra {
                inta[i], _ = strconv.Atoi(v)
        }
        return inta
}

func main() {
        stdin := bufio.NewScanner(os.Stdin)
        stdin.Scan()
        n, _ := strconv.Atoi(stdin.Text())
        is := make([]Arr, n)
        for i := 0; i <= n-1; i++ {
                stdin.Scan()
                var a Arr
                a.Val = ToIntSlice(strings.Split(stdin.Text(), " "))
                is[i] = a
        }
