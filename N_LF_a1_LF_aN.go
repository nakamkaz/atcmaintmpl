package main
        /*
                N
                a1
                a2
                a3
                ...
                aN
        */
import (
        "bufio"
        "fmt"
        "os"
        "strconv"
        //      "strings"
        //      "sort"
)

func main() {
        stdin := bufio.NewScanner(os.Stdin)
        stdin.Scan()
        n, _ := strconv.Atoi(stdin.Text())
        is := make([]int, n)
        for i := 0; i <= n-1; i++ {
                stdin.Scan()
                is[i], _ = strconv.Atoi(stdin.Text())
        }
        kmap := make(map[int]bool)
        for _, v := range is {
                kmap[v] = true
        }
}